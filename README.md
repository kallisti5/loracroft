# LoRa Croft

A healthy well-rounded LoRa test application

## Hardware Requirements

* Raspberry Pi or other small low power, Linux capable device
* SX1276 LoRa module such as the Adafruit RFM95W attached via SPI

## More Information

* https://lora-alliance.org/
* https://www.semtech.com/lora/what-is-lora
